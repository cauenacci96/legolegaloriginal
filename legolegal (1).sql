-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 03-Set-2019 às 02:48
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `legolegal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluguel_brinquedo`
--

DROP TABLE IF EXISTS `aluguel_brinquedo`;
CREATE TABLE IF NOT EXISTS `aluguel_brinquedo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crianca_id` int(11) NOT NULL,
  `plano_id` int(11) DEFAULT NULL,
  `valor_avulso` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `atualizado_em` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crianca_id` (`crianca_id`),
  KEY `plano_id` (`plano_id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `aluguel_brinquedo`
--

INSERT INTO `aluguel_brinquedo` (`id`, `crianca_id`, `plano_id`, `valor_avulso`, `usuario_id`, `status`, `criado_em`, `atualizado_em`) VALUES
(1, 1, 1, NULL, 2, 0, '2019-08-09 01:40:24', NULL),
(2, 10, 1, NULL, NULL, 0, '2019-08-18 21:08:29', NULL),
(3, 11, 1, NULL, NULL, 0, '2019-08-18 21:08:51', NULL),
(4, 12, 1, NULL, NULL, 0, '2019-08-18 21:08:44', NULL),
(5, 13, 1, NULL, NULL, 0, '2019-08-18 21:08:25', NULL),
(6, 14, 1, NULL, NULL, 0, '2019-08-21 05:08:56', NULL),
(7, 15, 1, NULL, NULL, 0, '2019-08-22 06:08:30', NULL),
(8, 17, 1, NULL, NULL, 0, '2019-09-03 05:09:09', NULL),
(9, 18, 1, NULL, NULL, 0, '2019-09-03 05:09:18', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `criancas`
--

DROP TABLE IF EXISTS `criancas`;
CREATE TABLE IF NOT EXISTS `criancas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nome_responsavel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_responsavel` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_nascimento` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `criado_em` datetime DEFAULT NULL,
  `atualizado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `criancas`
--

INSERT INTO `criancas` (`id`, `nome_completo`, `nome_responsavel`, `celular`, `email_responsavel`, `data_nascimento`, `criado_em`, `atualizado_em`) VALUES
(1, 'Cauê da Silva Nacci', 'Alice teste', '11974041151', NULL, '1996-08-08', '2019-08-08 22:40:03', NULL),
(2, 'nome', 'nome_responsavel', '1111111', 'dfdsfsd', '2019-08-02', '2019-08-08 14:42:00', NULL),
(3, 'NOme', 'Nome res', 'celular', 'email', 'data', '2019-08-18 18:08:48', NULL),
(4, 'hgjhg', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:31', NULL),
(5, 'hgjhg', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:53', NULL),
(6, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:12', NULL),
(7, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:23', NULL),
(8, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:37', NULL),
(9, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:41', NULL),
(10, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:29', NULL),
(11, 'caca', 'jhgjhg', 'jhgjhg', 'jhg', '00/00/0000', '2019-08-18 18:08:51', NULL),
(12, 'hgfhgf', 'hgfhgf', 'hg fh', '', 'gf/hg/hgf', '2019-08-18 18:08:44', NULL),
(13, 'vxcvcx', 'vxcvx', 'vc xvcx', 'vcxvxc@fsdfsd.fdsdsf', 'vc/xv/xcvc', '2019-08-18 18:08:25', NULL),
(14, 'hjgjgj', 'hgjhg', '45 64984-68', '', '65/46/5465', '2019-08-21 02:08:56', NULL),
(15, 'CauÃª', 'CauÃª Pai', '11 11111-1111', 'cauenacci@hotmail.com', '08/08/1996', '2019-08-22 03:08:30', NULL),
(16, '', '', '', 'gdf@fdrsrsd.dsds', '', '2019-09-03 02:09:36', NULL),
(17, 'gdfgdf', 'gfdgfd', '12 45', 'gtgfdgfd@fd.sdfds', '00/00/0000', '2019-09-03 02:09:09', NULL),
(18, 'Haras SÃ£o Luis', 'Haras SÃ£o Luis', '1128400416', 'cauenacci@hotmail.com', '12/32/1312', '2019-09-03 02:09:18', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `niveis_de_acesso`
--

DROP TABLE IF EXISTS `niveis_de_acesso`;
CREATE TABLE IF NOT EXISTS `niveis_de_acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nivel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `atualizado_em` timestamp NULL DEFAULT NULL,
  `criado_em` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `niveis_de_acesso`
--

INSERT INTO `niveis_de_acesso` (`id`, `nivel`, `status`, `atualizado_em`, `criado_em`) VALUES
(1, 'Administrador', 1, NULL, '2019-08-07 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `planos`
--

DROP TABLE IF EXISTS `planos`;
CREATE TABLE IF NOT EXISTS `planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_plano` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tempo` time NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `atualizado_em` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `planos`
--

INSERT INTO `planos` (`id`, `nome_plano`, `valor`, `tempo`, `status`, `criado_em`, `atualizado_em`) VALUES
(1, 'Ouro', '20', '00:10:00', 1, '2019-08-18 16:05:25', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `preco_minuto`
--

DROP TABLE IF EXISTS `preco_minuto`;
CREATE TABLE IF NOT EXISTS `preco_minuto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preco` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `atualizado_em` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `preco_minuto`
--

INSERT INTO `preco_minuto` (`id`, `preco`, `criado_em`, `atualizado_em`) VALUES
(1, '1', '2019-08-22 03:43:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nivel_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `atualizado_em` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nivel_id` (`nivel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `senha`, `nivel_id`, `status`, `criado_em`, `atualizado_em`) VALUES
(2, 'Cauê', 'cauenacci@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, '2019-08-06 23:59:36', NULL);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aluguel_brinquedo`
--
ALTER TABLE `aluguel_brinquedo`
  ADD CONSTRAINT `aluguel_brinquedo_ibfk_1` FOREIGN KEY (`crianca_id`) REFERENCES `criancas` (`id`),
  ADD CONSTRAINT `aluguel_brinquedo_ibfk_2` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`),
  ADD CONSTRAINT `aluguel_brinquedo_ibfk_3` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`nivel_id`) REFERENCES `niveis_de_acesso` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
