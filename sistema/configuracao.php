<!-- Segunrança -->
<?php include('../seg.php'); ?>

<!--incluir o header na página-->
<?php include('header.php'); ?>

<!--incluir o menu na página-->
<?php include('menu.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="card" style="box-shadow: 0px 0px 15px #0000002b;">
				<div class="card-header">
					<div class="row">
						<div class="col-md-11">
						    Planos
						</div>
						<div class="col-md-1">
                            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#cadastroUsuarioModal">
                                <i class="fas fa-plus"></i>
                            </button>
						</div>
                        <div class="col-md-12">
                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==true) { ?>
                                <div class="alert alert-success" id="cadastro">
                                    <!-- <i class="icon-thumbs-up"></i> -->
                                    <strong>Cadastrado com sucesso!</strong>
                                </div>
                            <?php } ?>

                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==false) { ?>
                                <div class="alert alert-danger" id="cadastro">
                                    <strong>Não foi possível cadastrar!</strong>
                                </div>
                            <?php } ?>
                        </div>
					</div>
				</div>
				  	
                <div class="card-body">
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
						<thead>
							<tr>
                                <th>Plano</th>
                                <th>Valor</th>
                                <th>Tempo</th>
                                <th>Status</th>
                                <th>Ação</th>
							</tr>
						</thead>
						<tbody>
                            <?php 
                            $queryPlanos = "SELECT * FROM `planos`";
                            $resPlanos = $pdo->query($queryPlanos);
                            while ($plano = $resPlanos->fetch()) { ?>
							<tr>
							    <td><?php echo $plano['nome_plano'];?></td>
                                <td><?php echo $plano['valor'];?></td>
                                <td><?php echo $plano['tempo'];?></td>
                                <td>
                                    <?php 
                                    if ($plano['status'] == 1) { ?>
                                        <span class="badge badge-pill badge-success">Ativo</span>
                                    <?php } else { ?>
                                        <span class="badge badge-pill badge-dark">Inativo</span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#alterarUsuario"><i class="fas fa-pencil-alt"></i></button>
                                    <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#deletarUsuario"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php } ?>			
						</tbody>
					</table>
				</div>
			</div>
		</div>

        <div class="col-md-4">
            <div class="card" style="box-shadow: 0px 0px 15px #0000002b;">
				<div class="card-header">
					<div class="row">
						<div class="col-md-10">
						    Preço/Minuto
						</div>
                        <?php $sqlPrecoMinuto = "SELECT * FROM `preco_minuto` WHERE `id` = 1";
                            $resPrecoMinuto = $pdo->query($sqlPrecoMinuto);
                            $precoMinuto = $resPrecoMinuto->fetch();
                        if ($precoMinuto === 0) { ?>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#cadastroUsuarioModal">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        <?php } ?>
					</div>
				</div>
				  	
                <div class="card-body">
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
						<thead>
							<tr>
                                <th>Preço</th>
                                <th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							    <td><?php echo $precoMinuto['preco'];?></td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#alterarUsuario"><i class="fas fa-pencil-alt"></i></button>
                                </td>
                            </tr>
						</tbody>
					</table>
				</div>
			</div>
        </div>
	</div>
</div>

<!--incluir o modal de cadastro de usuario-->
<?php include('usuario-modals/cadastro-usuario.php'); ?>

<!--incluir o footer na página-->
<?php include('footer.php'); ?>