<!-- Modal -->
<div class="modal fade" id="cadastroUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cadastrar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" action="queries/cadastro.php">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Nome <span style="color:#f00;">*</span></label>
                                 <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail <span style="color:#f00;">*</span></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="senha">Senha <span style="color:#f00;">*</span></label>
                                <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nivel">Nível <span style="color:#f00;">*</span></label>
                                <select class="form-control" id="nivel" name="nivel" required>
                                    <option value="">--Selecione um Nível--</option>
                                    <?php $statusAtivo = 1; 
                                    $queryNiveis = "SELECT * FROM niveis_de_acesso where `status` = '$statusAtivo'";
                                    $resNiveis = $pdo->query($queryNiveis);
                                    while ($nivel = $resNiveis->fetch()) { ?>
                                        <option value="<?php echo $nivel['id'];?>"> <?php echo $nivel['nivel'];?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="status" name="status">
                                    <label class="custom-control-label" for="status">Status</label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-2">
                            <div class="col-auto my-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">Selecione o valor</label>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="valor">Valor</label>
                                <input type="number" class="form-control" id="valor" name="valor" placeholder="12">
                            </div>
                        </div> -->
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
                </form>
        </div>
    </div>
</div>
