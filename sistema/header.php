
<?php 
include ("../conexaobanco.php");
$pdo = conectar();
?>

<!DOCTYPE html>
<html lang="pt-br" >

<head>
  <meta charset="utf-8"/>
  <title>Lego Legal</title>
    <!--Link do Jquery *sempre ficar em primeiro*-->
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> 

    <!--Link do Bootstrap online-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link rel="shortcut icon" type="image/x-icon" href="images/icone.ico">
    <!--Link do css geral e criado por nós-->
    <link rel="stylesheet" href="style.css">

    <!--Link do fontawesome online, para usar icones no sistema-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!--Links usados para os datatables-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

    <script language="JavaScript" src="cronometro.js"></script>

    <!--Código para configurações das datatables-->
    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "pageLength": 25,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Nenhum registro encontrado",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "enhum registro disponível",
                    "infoFiltered": "(Filtrado de _MAX_ total de registros)",
                    "search": "Buscar",
                    "paginate": {
                        first:      "Primeira",
                        previous:   "Anterior",
                        next:       "Próximo",
                        last:       "Última"
                    },
                },
                responsive: true
            } );
        } );
    </script>

    <script>
        function formatar(mascara, documento){
        var i = documento.value.length;
        var saida = mascara.substring(0,1);
        var texto = mascara.substring(i)
            if (texto.substring(0,1) != saida){
                documento.value += texto.substring(0,1);
            }
    }
    </script>

    <script>
        $().ready(function() {
            setTimeout(function () {
                $('#cadastro').hide(); // "foo" é o id do elemento que seja manipular.
            }, 3000); // O valor é representado em milisegundos.
        });
    </script>
</head>