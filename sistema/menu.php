<body style="background-image: url(imagens/legolegal.jpg)">
  <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
		    <a class="navbar-brand" href="dashboard.php">Lego Legal</a>
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="historico.php">Histórico</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="usuarios.php">Usuários</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="configuracao.php">Configuração</a>
                    </li>
                </ul>
                <span class="navbar-text">
                <a class="dropdown-item tleft" href="../logout.php">Sair</a>
                </span>
		    </div>
		</nav>
    </div>