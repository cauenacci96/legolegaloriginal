<!-- Modal -->
<div class="modal fade" id="cadastroAluguelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cadastrar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" action="queries/cadastro.php">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nomecompleto">Nome Completo <span style="color:#f00;">*</span></label>
                                 <input type="text" class="form-control" id="nomecompleto" name="nomecompleto" placeholder="Nome Completo" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nomeresponsavel">Nome do Responsável <span style="color:#f00;">*</span></label>
                                <input type="text" class="form-control" id="nomeresponsavel" name="nomeresponsavel" placeholder="Nome do Responsável" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="celular">Celular <span style="color:#f00;">*</span></label>
                                <input type="text" class="form-control" id="celular" name="celular" placeholder="(11) 00000-0000" OnKeyPress="formatar('## #####-####', this)" maxlength="13" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail </label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="datanasc">Data de Nascimento <span style="color:#f00;">*</span></label>
                                <input type="text" class="form-control" id="datanasc" name="datanasc" placeholder="00/00/0000" OnKeyPress="formatar('##/##/####', this)" maxlength="10" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="plano">Plano <span style="color:#f00;">*</span></label>
                                <select class="form-control" id="plano" name="plano" required>
                                    <option value="">--Selecione um plano--</option>
                                    <!-- Select de planos -->

                                    <?php $statusAtivo = 1; 
                                    $queryPlanos = "SELECT * FROM planos where `status` = '$statusAtivo'";
                                    $resPlanos = $pdo->query($queryPlanos);
                                    while ($plano = $resPlanos->fetch()) { ?>
                                        <option value="<?php echo $plano['id'];?>"> <?php echo $plano['nome_plano'];?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="col-md-2">
                            <div class="col-auto my-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">Selecione o valor</label>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="valor">Valor</label>
                                <input type="number" class="form-control" id="valor" name="valor" placeholder="12">
                            </div>
                        </div> -->
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
                </form>
        </div>
    </div>
</div>
