<!-- Segunrança -->
<?php include('../seg.php'); ?>

<!--incluir o header na página-->
<?php include('header.php'); ?>

<!--incluir o menu na página-->
<?php include('menu.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card" style="box-shadow: 0px 0px 15px #0000002b;">
				<div class="card-header">
					<div class="row">
						<div class="col-md-11">
						    Crianças
						</div>
						<div class="col-md-1">
                            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#cadastroAluguelModal">
                                <i class="fas fa-user-plus"></i>
                            </button>
						</div>
                        <div class="col-md-12 mt-2">
                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==true) { ?>
                                <div class="alert alert-success" id="cadastro">
                                    <!-- <i class="icon-thumbs-up"></i> -->
                                    <strong>Cadastrado com sucesso!</strong>
                                </div>
                            <?php } ?>

                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==false) { ?>
                                <div class="alert alert-danger" id="cadastro">
                                    <strong>Não foi possível cadastrar!</strong>
                                </div>
                            <?php } ?>
                        </div>
					</div>
				</div>
				  	
                <div class="card-body">
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
						<thead>
							<tr>
                                <th>Criança</th>
                                <th><i class="fas fa-clock"></i></th>
                                <th><i class="fas fa-dollar-sign"></i></th>
                                <th><i class="fas fa-users"></i></th>
                                <th><i class="fas fa-phone"></i></th>
                                <th>Ação</th>
							</tr>
						</thead>
						<tbody>
                            <?php $queryAlugueis = "SELECT * 
                            FROM aluguel_brinquedo 
                            INNER JOIN criancas ON criancas.id = aluguel_brinquedo.crianca_id
                            INNER JOIN planos ON planos.id = aluguel_brinquedo.plano_id";

                            $resAlugueis = $pdo->query($queryAlugueis);
                            while ($aluguel = $resAlugueis->fetch()) { ?>
							<tr>
							    <td><?php echo $aluguel['nome_completo'];?></td> 
                                <td><span class="badge badge-pill badge-info"><?php echo date("d/m/Y H:m", strtotime($aluguel['criado_em']));?></span></td>
                                <td><span class="badge badge-pill badge-secondary">R$ <?php echo $aluguel['valor'];?></span></td>
                                <td><span class="badge badge-pill badge-dark"><?php echo $aluguel['nome_responsavel'];?></span></td>
                                <td><span class="badge badge-pill badge-dark"><?php echo $aluguel['celular'];?></span></td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                        Finalizar
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                            <!-- <tr>
							    <td>João da Silva</td>
                                <td><span class="badge badge-danger badge-pill">00:27</span></td>
                                <td><span class="badge badge-pill badge-secondary">R$ 18,50</span></td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                        Finalizar
                                    </button>
                                </td>
                            </tr>
						    <tr>
                                <td>Fernando Costa</td>
                                <td><span class="badge badge-success badge-pill">00:34</span></td>
                                <td><span class="badge badge-pill badge-secondary">R$ 20,00</span></td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                                        Finalizar
                                    </button>
                                </td>
                            </tr>			 -->
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--incluir o modal de cadastro de aluguel-->
<?php include('dashboard-modals/cadastro-aluguel.php'); ?>

    <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nome da Criança</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5><b>Tempo brincando:</b> 00:27</h5>
                        <!-- <div class="alert alert-primary" role="alert">
                        <a href="#" class="alert-link">Tempo brincando: </a> 00:27
                        </div> -->
                        <h3><b>Total:  </b><span class="badge badge-pill badge-secondary"> R$ 18,50 </span></h3>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Finalizar</button>
                    </div>
                </div>
            </div>
        </div>
      
<!--incluir o footer na página-->
<?php include('footer.php'); ?>