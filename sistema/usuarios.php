<!-- Segunrança -->
<?php include('../seg.php'); ?>

<!--incluir o header na página-->
<?php include('header.php'); ?>

<!--incluir o menu na página-->
<?php include('menu.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card" style="box-shadow: 0px 0px 15px #0000002b;">
				<div class="card-header">
					<div class="row">
						<div class="col-md-11">
						    Usuários
						</div>
						<div class="col-md-1">
                            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#cadastroUsuarioModal">
                                <i class="fas fa-user-plus"></i>
                            </button>
						</div>
                        <div class="col-md-12 mt-2">
                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==true) { ?>
                                <div class="alert alert-success" id="cadastro">
                                    <!-- <i class="icon-thumbs-up"></i> -->
                                    <strong>Cadastrado com sucesso!</strong>
                                </div>
                            <?php } ?>

                            <?php if(isset($_GET["ins"]) && $_GET["ins"]==false) { ?>
                                <div class="alert alert-danger" id="cadastro">
                                    <strong>Não foi possível cadastrar!</strong>
                                </div>
                            <?php } ?>
                        </div>
					</div>
				</div>
				  	
                <div class="card-body">
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
						<thead>
							<tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Nível</th>
                                <th>Status</th>
                                <th>Ação</th>
							</tr>
						</thead>
						<tbody>
                            <?php $queryUsuarios = "SELECT * FROM usuarios
                            INNER JOIN niveis_de_acesso ON niveis_de_acesso.id = usuarios.nivel_id";

                            $resUsuarios = $pdo->query($queryUsuarios);
                            while ($usuario = $resUsuarios->fetch()) { ?>
							<tr>
							    <td><?php echo $usuario['nome'];?></td>
                                <td><?php echo $usuario['email'];?></td>
                                <td><?php echo $usuario['nivel'];?></td>
                                <td>
                                    <?php 
                                    if ($usuario['status'] == 1) { ?>
                                        <span class="badge badge-pill badge-success">Ativo</span>
                                    <?php } else { ?>
                                        <span class="badge badge-pill badge-dark">Inativo</span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#alterarUsuario"><i class="fas fa-pencil-alt"></i></button>
                                    <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#deletarUsuario"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php } ?>			
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--incluir o modal de cadastro de usuario-->
<?php include('usuario-modals/cadastro-usuario.php'); ?>

<!--incluir o footer na página-->
<?php include('footer.php'); ?>