<?php 

include ("conexaobanco.php");
$pdo = conectar();
?>

<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Jobprof" />

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="style.css" type="text/css" />
<link rel="stylesheet" href="css/swiper.css" type="text/css" />
<link rel="stylesheet" href="css/dark.css" type="text/css" />
<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css" />
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
<link rel="stylesheet" href="css/responsive.css" type="text/css" />
<link rel="stylesheet" href="demos/construction/css/colors.css" type="text/css" />
<link rel="stylesheet" href="demos/construction/construction.css" type="text/css" />
<link rel="stylesheet" href="css/components/bs-select.css" type="text/css" />

<link rel="shortcut icon" type="image/x-icon" href="icone.ico">

<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">


<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>Battec</title>

<?php //include('plugins/mascara_campos.php'); ?>

</head>
<body class="stretched">

<?php //include('login.php'); ?>

<div id="wrapper" class="clearfix">

<?php //if(isset($_GET['logout']) == True AND $_GET['logout'] == 1){ ?>
	
	<!-- Se o usuário foi desconectado -->
	
	<!--<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="icon-check"></i><strong>Sucesso!</strong> Usuário <a href="#" class="alert-link">desconectado.</a>.
	</div>-->
	
<?php //} ?>

<?php //if(isset($_GET['recuperar-senha']) == True AND $_GET['recuperar-senha'] == 1){ ?>
	
	<!-- Se o usuário foi desconectado -->
	
	<!--<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="icon-check"></i><strong>Sucesso!</strong> E-mail de redefinição enviado para o e-mail cadastrado.<a href="#" class="alert-link"></a>.
	</div>-->
	
<?php //} ?>

<?php //if(isset($_GET['senha']) == True AND $_GET['senha'] == 1){ ?>
	
	<!-- Se o usuário foi desconectado -->
	
	<!--<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="icon-check"></i><strong>Sucesso!</strong> Senha Redefinida com sucesso.<a href="#" class="alert-link"></a>.
	</div>-->
	
<?php //} ?>

<div id="top-bar">
	<div class="container clearfix">
		<div class="col_half nobottommargin clearfix">

			<div id="top-social">
				<ul>
					<li><a href="" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
					<li><a href="" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
					<li><a href="" class="si-linkedin"><span class="ts-icon"><i class="icon-linkedin"></i></span><span class="ts-text">Linkedin</span></a></li>
					<li><a href="" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
				</ul>
			</div>
		</div>
	<div class="col_half fright col_last clearfix nobottommargin">
		<div class="top-links">
			<ul>
				<li><a href="tel:11984021199"><i class="icon-call"></i> (11) 9 8402-1199</a></li>
				<li><a href="maito:battec@battec.com.br"><i class="icon-envelope-alt"></i> battec@battec.com.br</a></li>
			</ul>
		</div>
	</div>
	</div>
</div>

<header id="header" class="full-header">
	<div id="header-wrap" style="background:#000;">
		<div class="container clearfix">
			<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

			<div id="logo">
				<a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Battec Logo"></a>
				<a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo%402x.png" alt="Canvas Logo"></a>
			</div>
			
			<nav id="primary-menu">
				
				<ul>
					<li class="current mega-menu"><a href="index.php"><div>Home</div></a></li>
					<li><a href=""><div>Serviços</div></a>
						<ul>
						<?php $sqlservice = "SELECT * FROM `servicos`";
						$resservice = $pdo->query($sqlservice);
						while($service = $resservice->fetch()){
							$idservice = $service['id_servico'];
							$nomeservice = $service['servico']; 
								if (isset($nomeservice) == True){ ?>
									<li><a href="servico.php?id_servico=<?php echo $idservice ?>"><div><?php echo utf8_encode($nomeservice)?></div></a></li>
								<?php }
						} ?>
						</ul>
					</li>
					<li><a href="contato.php"><div>Contato</div></a></li>
					<li><a href="acesso.php"><div>Acesso</div></a></li>
						
					
				</ul>	

				<!--<div id="top-search">
				<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
				<form action="http://themes.semicolonweb.com/html/canvas/search.html" method="get">
				<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
				</form>
				</div>-->
				
				<div id="top-search">
				<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
				<form action="resultado_pesquisa.php" method="get">
					<input type="text" name="pesquisa" class="form-control" value="" placeholder="O que você procura..">
				</form>
			</div>

			</nav>
		</div>
	</div>
</header>


<style>

	.revo-slider-emphasis-text {
		font-size: 64px;
		font-weight: 700;
		letter-spacing: -1px;
		font-family: 'Raleway', sans-serif;
		padding: 15px 20px;
		border-top: 2px solid #FFF;
		border-bottom: 2px solid #FFF;
	}

	.revo-slider-desc-text {
		font-size: 20px;
		font-family: 'Lato', sans-serif;
		width: 650px;
		text-align: center;
		line-height: 1.5;
	}

	.revo-slider-caps-text {
		font-size: 16px;
		font-weight: 400;
		letter-spacing: 3px;
		font-family: 'Raleway', sans-serif;
	}
	.tp-video-play-button { display: none !important; }

	.tp-caption { white-space: nowrap; }

</style>

<section id="slider" class="slider-element revslider-wrap clearfix" data-loop="true" style="height: 550px;">
	<div class="slider-parallax-inner">
		<div id="rev_slider_679_1_wrapper" class="rev_slider_wrapper fullwidth-container" style="padding:0px;">
			<div id="rev_slider_679_1" class="rev_slider fullwidthbanner" style="display:none;" data-version="5.1.4">
				<ul> 
					
					<?php 
					
					$sql_slide = "SELECT * FROM `slide`";
					$res_slide = $pdo->query($sql_slide);
					while ($reg_slide = $res_slide->fetch()) {
					
					?>
					
					<li class="dark" data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="slides/<?php echo $reg_slide['arquivo']; ?>" data-delay="10000" data-saveperformance="off" data-title="Responsive Design">

						<img src="slides/<?php echo $reg_slide['arquivo']; ?>" alt="kenburns1" data-bgposition="center bottom" data-bgpositionend="center top" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
						
						<div class="slider-caption slider-caption-center">
						<h2><?php echo $reg_slide['titulo']; ?></h2>
						<p class="d-none d-sm-block"><?php echo $reg_slide['descricao']; ?></p>
						</div><i class="icon-angle-right"></i>			
										
					</li>
					
					<?php } ?>

				</ul>
			</div>
		</div>
	</div>
</section>



<section id="content">
	<div class="content-wrap">
		<div class="promo promo-full promo-border header-stick bottommargin-lg">
			<div class="container clearfix">
				
				<h3>Entre em contato hoje <span><a href="https://api.whatsapp.com/send?phone=5511984021199">(11) 9 8402-1199</a></span> ou por E-mail <span><a href="mailto:battec@battec.com.br">battec@battec.com.br</a></span></h3>
				<span>Nós nos esforçamos para entregar aos nossos clientes maior qualidade no atendimento e relacionamento.</span>
				<a href="contato.php" class="button button-reveal button-xlarge button-rounded tright"><i class="icon-angle-right"></i><span>Fale Agora</span></a>
			</div>
		</div>
		<div class="container clearfix">
			<?php
			
			$ultima_coluna = null;
			$cont = 1;
			
			$sqlservice2 = "SELECT * FROM `servicos`";
				$resservice2 = $pdo->query($sqlservice2);
					while($service2 = $resservice2->fetch()){
						$idservice2 = $service2['id_servico'];
						$nomeservice2 = $service2['servico'];
						//$brevedesservice2 = $service2['brevedesc']; ?>
							<div class="col_one_third">
								<div class="feature-box fbox-effect">
									<div class="fbox-icon">
										<a href="servico.php?id=<?php echo $idservice2 ?>"><i class="icon-screen i-alt"></i></a>
									</div>
									<h3><?php echo utf8_encode($nomeservice2) ?></h3>
									
								</div>
							</div>				
					<?php } ?>		
		<div class="clear"></div>	
		<div class="line"></div>	
		</div>
	</div>
	
</section>

<footer id="footer" class="dark">
	<div class="container">
		<div class="footer-widgets-wrap clearfix">
			<div class="col_two_third">
				<div class="col_one_third">
					<div class="widget clearfix">
						<img src="images/logo.png" alt="" class="footer-logo">
						<!-- <p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards.</p> -->
						<div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
							<abbr title="Contato"><strong>Telefone:</strong></abbr> (11) 98402-1199<br>
							<abbr title="Contato"><strong>Telefone:</strong></abbr> (11) 99002-2303<br>
							<abbr title="Email"><strong>Email:</strong></abbr> <a href="battec@battec.com.br">battec@battec.com.br</a>
						</div>
					</div>
				</div>
				<div class="col_one_third">
					<!--<div class="widget widget_links clearfix">
						<h4>Acesso Rápido</h4>
						<ul>
							<li><a href="sobre.php">Institucional</a></li>
							<li><a href="login-cliente.php">Área do Cliente</a></li>
							<li><a href="login-corretor.php">Área do Corretor</a></li>
							<li><a href="login-investidor.php">Área do Investidor</a></li>
							<li><a href="login-colaborador.php">Seja Nosso colaborador</a></li>
							<li><a href="login-colaborador.php"><strong>Portal:</strong> Preço Sinalização</a></li>
							<li><a href="login-colaborador.php"><strong>Portal:</strong> Sua Casa Nova</a></a></li>
						</ul>
					</div>-->
				</div>
				<div class="col_one_third col_last">
					<div class="widget clearfix">
						<h4>Notícias Recentes</h4>
						<div id="post-list-footer">
							<div class="spost clearfix">
								<?php 
								
								//$sql = "SELECT * FROM blog WHERE id_organizacao = $id_organizacao_ativa ORDER BY id DESC LIMIT 3";
								//$res = $pdo->query($sql);
								//while($post_recentes = $res->fetch()){								
								
								?>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="post.php?id=<?php //echo $post_recentes['id']; ?>"><?php //echo $post_recentes['titulo']; ?></a></h4>
									</div>
									<ul class="entry-meta">
										<li><?php //echo $post_recentes['data_postagem']; ?></li>
									</ul>
								</div>
								
								<?php //} ?>
								
							</div>					
						</div>
					</div>
				</div>
			</div>
			<div class="col_one_third col_last">
				<!-- <div class="widget clearfix" style="margin-bottom: -20px;">
					<div class="row">
						<div class="col-lg-6 bottommargin-sm">
							<div class="counter counter-small"><span data-from="50" data-to="15065421" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
							<h5 class="nobottommargin">Total de Projetos</h5>
						</div>
						<div class="col-lg-6 bottommargin-sm">
						<div class="counter counter-small"><span data-from="100" data-to="18465" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
							<h5 class="nobottommargin">Clientes</h5>
						</div>
					</div>
				</div> -->
				<div class="widget clearfix">
					<h5><strong>Inscreva-se</strong> receba notícias e oportunidades de nossos parceiros.</h5>
					<form action="admin/cadastros/bd/cadastrar_lead_banco.php" method="post">
					
						<input type="hidden" name="nome" value="">
						<input type="hidden" name="sobrenome" value="">
						<input type="hidden" name="id_organizacao_ativa" value="<?php //echo $id_organizacao_ativa; ?>">
						<input type="hidden" name="celular" value="">
						<input type="hidden" name="pagina_retorno_concluido" value="http://www.battec.com.br?cadastrado=1">
						<input type="hidden" name="pagina_retorno_erro" value="http://www.battec.com.br?cadastrado=0">
					
						<div class="input-group divcenter">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="icon-email2"></i></div>
							</div>
							<input type="email" name="email" class="form-control required email" placeholder="Entre com seu e-mail" required>
							<div class="input-group-append">
								<button class="btn btn-success" type="submit">Inscreva-se</button>
							</div>
						</div>
					</form>
					
				</div>
				<div class="widget clearfix" style="margin-bottom: -20px;">
					<div class="row">
						<div class="col-lg-6 clearfix bottommargin-sm">
							<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>
							<a href="#"><small style="display: block; margin-top: 3px;"><strong>Curta nossa página</strong><br>no Facebook</small></a>
						</div>
						<div class="col-lg-6 clearfix">
							<!-- <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
								<i class="icon-rss"></i>
								<i class="icon-rss"></i>
							</a>
							<a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div id="copyrights">
<div class="container clearfix">
<div class="col_half">
Copyrights &copy; 2018 Todos os direitos reservados a Battec Serv. de Engen. Eireli.<br>
<!--<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>-->
</div>
<div class="col_half col_last tright">
<div class="fright clearfix">
<a href="https://www.facebook.com/Battec-Engenharia-Manuten%C3%A7%C3%A3o-1079849162183543/" class="social-icon si-small si-borderless si-facebook">
<i class="icon-facebook"></i>
<i class="icon-facebook"></i>
</a>
<a href="https://twitter.com/BattecEngenhar1" class="social-icon si-small si-borderless si-twitter">
<i class="icon-twitter"></i>
<i class="icon-twitter"></i>
</a>
<!--<a href="#" class="social-icon si-small si-borderless si-gplus">
<i class="icon-gplus"></i>
<i class="icon-gplus"></i>
</a>-->
<a href="https://www.instagram.com/battecengenharia/" class="social-icon si-small si-borderless si-instagram">
<i class="icon-instagram2"></i>
<i class="icon-instagram2"></i>
</a>
<!--<a href="#" class="social-icon si-small si-borderless si-youtube">
<i class="icon-youtube"></i>
<i class="icon-youtube"></i>
</a>-->
<a href="https://www.linkedin.com/company/battecengenharia/" class="social-icon si-small si-borderless si-linkedin">
<i class="icon-linkedin"></i>
<i class="icon-linkedin"></i>
</a>
</div>

</div>
</div>
</div>
</footer>

<style type="text/css">
.button-whatsapp {
   position: fixed;
   bottom: 40px;
   width: 50px;
   z-index: 100;
   left: 20px;
}
</style>

	
<div class="clear"></div>


<div id="gotoTop" class="icon-angle-up"></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>
<script src="js/components/bs-select.js"></script>

<script src="js/functions.js"></script>

<script src="include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script>

		var tpj=jQuery;
		var revapi31;
		tpj(document).ready(function() {
			if(tpj("#rev_slider_679_1").revolution == undefined){
				revslider_showDoubleJqueryError("#rev_slider_679_1");
			}else{
				revapi31 = tpj("#rev_slider_679_1").show().revolution({
					sliderType:"standard",
					jsFileLocation:"include/rs-plugin/js/",
					sliderLayout:"fullwidth",
					dottedOverlay:"none",
					delay:16000,
					hideThumbs:200,
					thumbWidth:100,
					thumbHeight:50,
					thumbAmount:5,
					navigation: {
	                    keyboardNavigation: "on",
	                    keyboard_direction: "horizontal",
	                    mouseScrollNavigation: "off",
	                    onHoverStop: "off",
	                    touch: {
	                        touchenabled: "on",
	                        swipe_threshold: 75,
	                        swipe_min_touches: 1,
	                        swipe_direction: "horizontal",
	                        drag_block_vertical: false
	                    },
	                    arrows: {
	                        style: "hades",
	                        enable: true,
	                        hide_onmobile: false,
	                        hide_onleave: false,
	                        tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
	                        left: {
	                            h_align: "left",
	                            v_align: "center",
	                            h_offset: 10,
	                            v_offset: 0
	                        },
	                        right: {
	                            h_align: "right",
	                            v_align: "center",
	                            h_offset: 10,
	                            v_offset: 0
	                        }
	                    },
	                },
					responsiveLevels:[1140,1024,778,480],
					visibilityLevels:[1140,1024,778,480],
					gridwidth:[1140,1024,778,480],
					gridheight:[700,768,960,720],
					lazyType:"none",
					shadow:0,
					spinner:"off",
					stopLoop:"off",
					stopAfterLoops:-1,
					stopAtSlide:-1,
					shuffle:"off",
					autoHeight:"off",
					fullScreenAutoWidth:"off",
					fullScreenAlignForce:"off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "0px",
					hideThumbsOnMobile:"off",
					hideSliderAtLimit:0,
					hideCaptionAtLimit:0,
					hideAllCaptionAtLilmit:0,
					debugMode:false,
					fallbacks: {
						simplifyAll:"off",
						nextSlideOnWindowFocus:"off",
						disableFocusListener:false,
					}
				});
			}

            revapi31.bind("revolution.slide.onloaded",function (e) {
				setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
			});

			revapi31.bind("revolution.slide.onchange",function (e,data) {
				SEMICOLON.slider.revolutionSliderMenu();
			});
		});	/*ready*/
	</script>
	
</body>

<!-- Mirrored from themes.semicolonweb.com/html/canvas/header-dark.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 05 Oct 2018 14:40:21 GMT -->
</html>