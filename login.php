<div id="side-panel">
	<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>
	<div class="side-panel-wrap">
		<div class="widget clearfix">
			<!--<h4 class="t400">Logar com o seu perfil social</h4>
			<a href="#" class="button button-rounded t400 btn-block center si-colored noleftmargin si-facebook">Facebook</a>
			<a href="#" class="button button-rounded t400 btn-block center si-colored noleftmargin si-gplus">Google</a>
			<br><br> -->
			<h4 class="t400">Possui uma conta?</h4>
			<form id="login-form" name="login-form" class="nobottommargin" action="admin/bd/valida-login.php" method="post">
				<input type="hidden" name="id_organizacao_ativa" value="10">
				<input type="hidden" name="portal" value="cliente">
				<input type="hidden" name="id_permissao" value="2,3,4,5,6,7,8">
				<input type="hidden" name="pagina_retorno" value="../../login-colaborador.php">
				
				<div class="col_full">
					<label for="login-form-username" class="t400">E-mail:</label>
					<input type="text" id="login-form-username" name="email" value="" class="form-control" />
				</div>
				<div class="col_full">
					<label for="login-form-password" class="t400">Senha:</label>
					<input type="password" id="login-form-password" name="senha" value="" class="form-control" />
				</div>
				<div class="col_full nobottommargin">
					<button class="button button-rounded t400 nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
					<br><br>
					<a href="#" class="fleft"></a>
					<br><br>
					<a href="login-cliente.php" class="fleft">Não possui uma conta?</a>
				</div>
			</form>
		</div>
	</div>
</div>