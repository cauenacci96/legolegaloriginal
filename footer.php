<footer id="footer" class="dark">
	<div class="container">
		<div class="footer-widgets-wrap clearfix">
			<div class="col_two_third">
				<div class="col_one_third">
					<div class="widget clearfix">
						<img src="images/logo.png" alt="" class="footer-logo">
						<div>
							<address>
								<strong>São Paulo:</strong><br>
								R. Eugênio de Freitas, 371, Apto 92B<br>
								Vila Guilherme<br>
								São Paulo, SP - 02060-000<br>
							</address>
							<address>
								<strong>Salto:</strong><br>
								Av. da Padroeira, 154<br>
								Jd. São Gabriel II<br>
								Salto, SP - 13327-543<br>
							</address>
							<strong>Telefone:</strong> (11) 98402-1199<br>
							<strong>Telefone:</strong> (11) 99002-2303<br>
							<strong>Email:</strong> <a href="battec@battec.com.br">battec@battec.com.br</a>
						</div>
					</div>
				</div>
				<div class="col_one_third">
					<div class="widget widget_links clearfix">
						<h4>Acesso Rápido</h4>
						<ul>
							<?php if ($acesso1 == 1) { ?>
							<li><a href="dashboard.php">Home</a></li>
							<li><a href="projetos.php">Projetos</a></li>
							<?php } else { } ?>
							<!--<li><a href="login-corretor.php">Área do Corretor</a></li>
							<li><a href="login-investidor.php">Área do Investidor</a></li>
							<li><a href="login-colaborador.php">Seja Nosso colaborador</a></li>
							<li><a href="login-colaborador.php"><strong>Portal:</strong> Preço Sinalização</a></li>
							<!-- <li><a href="login-colaborador.php"><strong>Portal:</strong> Sua Casa Nova</a></a></li> -->
						</ul>
					</div>
				</div>
				<div class="col_one_third col_last">
					<div class="widget clearfix">
						<h4>Notícias Recentes</h4>
						<div id="post-list-footer">
							<div class="spost clearfix">
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
									</div>
									<ul class="entry-meta">
										<li>10th July 2014</li>
									</ul>
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<div class="col_one_third col_last">
				<div class="widget subscribe-widget clearfix">
					<h5><strong>Inscreva-se</strong> receba notícias semanais de nossas parceiros e oportunidades.</h5>
					<div class="widget-subscribe-form-result"></div>
					<form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" class="nobottommargin">
						<div class="input-group divcenter">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="icon-email2"></i></div>
							</div>
							<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Entre com seu e-mail">
							<div class="input-group-append">
								<button class="btn btn-success" type="submit">Inscreva-se</button>
							</div>
						</div>
					</form>
				</div>
				<div class="widget clearfix" style="margin-bottom: -20px;">
					<div class="row">
						<div class="col-lg-6 clearfix bottommargin-sm">
							<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>
							<a href="#"><small style="display: block; margin-top: 3px;"><strong>Curta nossa página</strong><br>no Facebook</small></a>
						</div>
						<div class="col-lg-6 clearfix"></div>
					</div>
				</div>
			</div>
</div>
</div>

<div id="copyrights">
<div class="container clearfix">
<div class="col_half">
Copyrights &copy; - 2019 | Todos os direitos reservados a Battec Serv. de Engen. Eireli.<br>
</div>
<div class="col_half col_last tright">
<div class="fright clearfix">
<a href="https://www.facebook.com/Battec-Engenharia-Manuten%C3%A7%C3%A3o-1079849162183543/" class="social-icon si-small si-borderless si-facebook">
<i class="icon-facebook"></i>
<i class="icon-facebook"></i>
</a>
<a href="https://twitter.com/BattecEngenhar1" class="social-icon si-small si-borderless si-twitter">
<i class="icon-twitter"></i>
<i class="icon-twitter"></i>
</a>
<!--<a href="#" class="social-icon si-small si-borderless si-gplus">
<i class="icon-gplus"></i>
<i class="icon-gplus"></i>
</a>-->
<a href="https://www.instagram.com/battecengenharia/" class="social-icon si-small si-borderless si-instagram">
<i class="icon-instagram2"></i>
<i class="icon-instagram2"></i>
</a>
<!--<a href="#" class="social-icon si-small si-borderless si-youtube">
<i class="icon-youtube"></i>
<i class="icon-youtube"></i>
</a>-->
<a href="https://www.linkedin.com/company/battecengenharia/" class="social-icon si-small si-borderless si-linkedin">
<i class="icon-linkedin"></i>
<i class="icon-linkedin"></i>
</a>
</div>
<!--<div class="clear"></div>
Desenvolvido por: <a href="www.jobprof.com.br">jobprof.com.br</a>
</div>-->
</div>
</div>
</footer>