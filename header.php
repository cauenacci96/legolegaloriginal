<header id="header" class="full-header">
	<div id="header-wrap" style="background:#000;">
		<div class="container clearfix">
			<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

			<div id="logo">
				<a href="" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Battec Logo"></a>
				<a href="" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo%402x.png" alt="Canvas Logo"></a>
			</div>
			
			<?php $sql = "SELECT * FROM `usuarios` WHERE `cpf`='$cpfsessao'";
				$res = $pdo->query($sql);
					$usuario = $res->fetch();
	
					$acesso1 = $usuario['primeiro_acesso']; ?>
			
			<nav id="primary-menu">
				<?php if ($acesso1 == 1) { ?>
				<ul>
					<li class="current mega-menu"><a href="dashboard.php"><div>Home</div></a></li>
					<li><a href="projetos.php"><div>Projetos</div></a></li>
					<?php if($permissaoacesso == 1){ ?> 
						<li><a href="usuarios.php"><div>Usuários</div></a></li>
						<li><a href=""><div>Site</div></a>
							<ul>
								<li><a href="slide.php"><div>Slides</div></a></li>
								<li><a href="servicos.php"><div>Serviços</div></a></li>
						
							</ul>
						</li>
					<?php } else {}?>
				</ul>
				<?php } else {} ?>

				<div id="top-account" class="dropdown">
							<a href="#" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon-user"></i></a>
					<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
							<?php if ($acesso1 == 1) { ?>
							<a class="dropdown-item tleft" href="perfil.php"><i class="icon-user"></i> Perfil</a>
							<!--<a class="dropdown-item tleft" href="#">Chamados <span class="badge badge-pill badge-secondary fright" style="margin-top: 3px;">5</span></a>-->
						<div class="dropdown-divider"></div>
							<a class="dropdown-item tleft" href="logout.php"><i class="icon-signout"></i> Sair</a>
							<?php } else { ?>
							<a class="dropdown-item tleft" href="logout.php"><i class="icon-signout"></i> Sair</a>	
							<?php } ?>
					</ul>
				</div>	

				<!--<div id="top-search">
				<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
				<form action="http://themes.semicolonweb.com/html/canvas/search.html" method="get">
				<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
				</form>
				</div>-->

			</nav>
		</div>
	</div>
</header>